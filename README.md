# Vérification des journaux de cluster et de nœud

------------

><img src="https://i.pinimg.com/280x280_RS/6b/68/be/6b68bed191fdd2fad36e4193e64764ee.jpg" width="50" height="50" alt="Carlin Fongang">

>Carlin FONGANG | fongangcarlin@gmail.com

>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://github.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______

# Objectifs

Nous allons parler de la vérification des journaux de cluster et de nœud. Voici un aperçu rapide de ce dont nous allons discuter :

- Journaux des services
- Journaux des composants du cluster
- Démonstration pratique

## Journaux des services

Les journaux des services sont une partie importante du dépannage d'un cluster Kubernetes. Vous pouvez vérifier les journaux de vos services liés à Kubernetes sur chaque nœud en utilisant `journalctl`. Par exemple, vous pouvez utiliser les commandes suivantes pour obtenir les journaux de kubelet et Docker respectivement :

```shell
$ sudo journalctl -u kubelet
$ sudo journalctl -u docker
```

## Journaux des composants du cluster

Les composants d'un cluster Kubernetes redirigent la sortie des journaux vers `/var/log`. Par exemple, vous pouvez consulter `/var/log/kube-apiserver.log` pour voir les journaux de votre serveur API Kubernetes. Vous devez être conscient de ces emplacements lorsque vous travaillez avec Kubernetes dans le monde réel. Cependant, notez que ces fichiers journaux n'apparaîtront pas pour les clusters créés avec `kubeadm`, comme le cluster que nous utilisons dans ce cours, et potentiellement même les clusters que vous utiliserez lors de l'examen Certified Kubernetes Administrator (CKA).

```shell
/var/log/kube-apiserver.log
/var/log/kube-scheduler.log
/var/log/kube-controller-manager.log
```

La raison pour laquelle ces journaux n'apparaissent pas dans un cluster `kubeadm` est que tous ces composants s'exécutent en tant que pods système. Ils ne s'exécutent pas directement sur l'hôte, mais à l'intérieur des conteneurs. Par conséquent, ces fichiers journaux ne seront pas présents sur l'hôte dans un cluster `kubeadm`. Cependant, dans un cluster `kubeadm`, vous pouvez toujours accéder à ces journaux en utilisant la commande `kubectl logs` sur ces pods système. Si cela ne vous semble pas clair, ne vous inquiétez pas, nous allons voir cela dans notre démonstration pratique.

## Démonstration pratique

Passons maintenant à une démonstration pratique pour examiner certains journaux dans notre cluster. Nous sommes connectés à notre nœud de plan de contrôle Kubernetes, et nous allons commencer par vérifier les journaux du service kubelet :

```shell
sudo journalctl -u kubelet
```

En appuyant sur `Shift+G`, nous pouvons sauter à la fin de nos journaux. Il y a beaucoup d'informations ici. Je ne vois actuellement aucune erreur ou problème, mais si quelque chose ne va pas avec kubelet, vous verrez probablement quelque chose de pertinent qui vous donnera plus d'informations sur ce qui se passe dans le journal de kubelet.

Nous avons parlé plus tôt des pods système kube-system. Utilisons la commande suivante pour voir ces pods :

```shell
kubectl get pods -n kube-system
```

De nombreux composants Kubernetes s'exécutent ici en tant que pods, par exemple, kube-apiserver. Que faire si je veux voir les journaux de mon serveur API Kubernetes ? Comme il s'exécute en tant que pod, je peux simplement utiliser la commande `kubectl logs` pour cela :

```shell
kubectl logs -n kube-system <nom-du-pod-kube-apiserver>
```

Ici, je peux voir les journaux de mon serveur API Kubernetes. Si vous avez besoin de voir les journaux de l'un de ces composants s'exécutant en tant que pods dans le cluster, vous pouvez le faire en utilisant la commande `kubectl logs`.

# Récapitulatif

Dans cette leçon, nous avons brièvement parlé de la consultation des journaux de cluster et de nœud. Nous avons examiné les journaux des services, les journaux des composants du cluster s'exécutant en tant que pods dans le cluster, puis nous avons fait une démonstration pratique pour localiser ces journaux. C'est tout pour cette leçon. Nous vous retrouverons dans la prochaine leçon.

# Reférence

https://kubernetes.io/docs/tasks/debug/debug-cluster/#looking-at-logs